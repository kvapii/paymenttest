package org.mytest;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.locks.Lock;

import org.apache.commons.collections.CollectionUtils;

/**
 * Reads lines from console
 *
 * Created by Pavel.Kvapil on 8.3.2015.
 */
public class CmdLineReader {

    private static final String END_APPLICATION = "quit";
    private static final String CAN_WRITE = "y";
    private static final String MSG_ENTER_DATA_QUESTION = "Chcete zadavat údaje? Zadejte Y a stiskněte enter.";
    private static final String MSG_WRITE_DATA = "Můžete zadávat data:";
    private static final String MSG_INVALID_INPUT = "InvalidInput:";

    /**
     * Read input from console
     */
    public void read(){

        final List<String> inputs = new ArrayList<String>();
        String input = null;
        final Lock writeLock = CmdLinePrinterLock.getInstance().getWriteLock();

        final Scanner in = new Scanner(System.in);
        try{
            in.useDelimiter(System.getProperty("line.separator"));
            print(MSG_ENTER_DATA_QUESTION);

            while (!END_APPLICATION.equalsIgnoreCase(input)){
                //read line
                input = in.nextLine();

                if (CAN_WRITE.equalsIgnoreCase(input)) {
                    writeLock.lock();
                    print(MSG_WRITE_DATA);
                    continue;
                }
                inputs.add(input);

                if(input.isEmpty()){
                    processInput(inputs);
                    writeLock.unlock();
                    print(MSG_ENTER_DATA_QUESTION);
                }
            }
        }finally {
            in.close();
            System.exit(1);
        }
    }

    /**
     * Process input strings
     * parse and print invalid input
     *
     * @param inputs to process by parser
     */
    private void processInput(final List<String> inputs) {
        final Parser parser = new Parser();
        final List<String> invalid = parser.parse(inputs);

        if (CollectionUtils.isNotEmpty(invalid)){
            print(MSG_INVALID_INPUT);
            for (String item : invalid){
                print(item);
            }
        }
        inputs.clear();
    }

    private void print(final String msg){
        System.out.println(msg);
    }
}
