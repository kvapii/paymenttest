package org.mytest.enums;

/**
 * Result of validation
 *
 * Created by Pavel.Kvapil on 7.3.2015.
 */
public enum ValidationStatus {

    OK,
    ERROR;
}
