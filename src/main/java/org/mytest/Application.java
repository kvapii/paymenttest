package org.mytest;


import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

/**
 * Main application class
 *
 * Created by Pavel.Kvapil on 7.3.2015.
 */
public class Application {

    private static final int JOB_INTERVAL_IN_SECONDS = 60;

    public void run(){

        init();
        final CmdLineReader cmdLineReader = new CmdLineReader();
        cmdLineReader.read();
    }
    /**
     * initialize application
     */
    private void init() {

        //registr print job
        registrJob();
    }


    private void registrJob(){

        final JobDetail job = JobBuilder.newJob(PrintJob.class)
                .withIdentity("printJob", "group1").build();

        final Trigger trigger = TriggerBuilder
                .newTrigger()
                .withIdentity("dummyTriggerName", "group1")
                .withSchedule(
                        SimpleScheduleBuilder.simpleSchedule()
                                .withIntervalInSeconds(JOB_INTERVAL_IN_SECONDS).repeatForever())
                .build();

        try {
            final Scheduler scheduler = new StdSchedulerFactory().getScheduler();
            scheduler.start();
            scheduler.scheduleJob(job, trigger);
        } catch (final SchedulerException e) {
            e.printStackTrace();
        }
    }

}
