package org.mytest;

import java.math.BigDecimal;

/**
 * Payment to save
 *
 * Created by Pavel.Kvapil on 8.3.2015.
 */
public class Payment {

    private String currency;
    private BigDecimal amount;

    public Payment(final String currency, final BigDecimal amount) {
        this.currency = currency;
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(final String currency) {
        this.currency = currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(final BigDecimal amount) {
        this.amount = amount;
    }
}
