package org.mytest;

import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.locks.Lock;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * Job for printing all currencies balance
 *
 * Created by Pavel.Kvapil on 7.3.2015.
 */
public class PrintJob implements Job {

    @Override
    public void execute(final JobExecutionContext jobExecutionContext) throws JobExecutionException {

        final Lock readLock = CmdLinePrinterLock.getInstance().getReadLock();
        readLock.lock();
        System.out.println("Tisk balance.....");
        for(Map.Entry<String,BigDecimal> currency: Account.getInstance().getCurrenciesBalance().entrySet()){
            if(BigDecimal.ZERO.compareTo(currency.getValue()) != 0){
                System.out.println(currency.getKey() + " " + currency.getValue());
            }
        }
        readLock.unlock();

    }
}
