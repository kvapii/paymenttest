package org.mytest;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by Pavel.Kvapil on 8.3.2015.
 */
public final class CmdLinePrinterLock {

    private static final CmdLinePrinterLock INSTANCE = new CmdLinePrinterLock();

    private static final AtomicBoolean ATOMIC_BOOLEAN = new AtomicBoolean(true);

    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

    private final Lock readLock = readWriteLock.readLock();
    private final Lock writeLock = readWriteLock.writeLock();

    private CmdLinePrinterLock() {
    }

    public Lock getReadLock(){
        return readLock;
    }

    public Lock getWriteLock(){
        return writeLock;
    }

    public static CmdLinePrinterLock getInstance(){
        return INSTANCE;
    }


    public void lock(){
        ATOMIC_BOOLEAN.getAndSet(false);
    }

    public void unlock(){
        ATOMIC_BOOLEAN.getAndSet(true);
    }

    public boolean canPrint(){
        return ATOMIC_BOOLEAN.get();
    }
}
