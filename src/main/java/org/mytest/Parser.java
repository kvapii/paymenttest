package org.mytest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.mytest.enums.ValidationStatus;

/**
 * Parse and validate input
 *
 * Created by Pavel.Kvapil on 7.3.2015.
 */
public class Parser {

    /**
     * Parse and save input
     * @param inputs
     * @return invalid inputs
     */
    public List<String> parse(final List<String> inputs) {

        if(CollectionUtils.isEmpty(inputs)){
            return null;
        }
        final List<String> invalidInputs = new ArrayList<String>();
        final List<Payment> payments = new ArrayList<Payment>();

        for(String input: inputs){
            if(StringUtils.isNotBlank(input)){
                if(ValidationStatus.ERROR.equals(validate(input))){
                    invalidInputs.add(input);
                } else {
                    payments.add(new Payment(getIsoCode(input), getAmount(input)));
                }
            }
        }
        Account.getInstance().updateBalance(payments);

        return invalidInputs;
    }

    /**
     * validate input string
     * @param input line from command line
     * @return validation status
     */
    private ValidationStatus validate(final String input){

        final String isoCode = getIsoCode(input);
        if(!StringUtils.isAlpha(isoCode) || !StringUtils.isAllUpperCase(isoCode)){
            return ValidationStatus.ERROR;
        }

        final String separator = StringUtils.substring(input,3,4);

        if(!StringUtils.containsAny(separator, "- ")){
            return ValidationStatus.ERROR;
        }
        final String amount = getAmountAsString(input);

        if (StringUtils.isEmpty(amount)){
            return ValidationStatus.ERROR;
        }

        try{
            final BigDecimal validAmount = NumberUtils.createBigDecimal(amount);
        } catch (final NumberFormatException ex){
            return ValidationStatus.ERROR;
        }

        return ValidationStatus.OK;
    }

    private String getIsoCode(final String input){
        return StringUtils.left(input,3);
    }

    private String getAmountAsString(final String input){
        return StringUtils.substring(input,4);
    }

    private BigDecimal getAmount(final String input){
        return NumberUtils.createBigDecimal(getAmountAsString(input));
    }

}
