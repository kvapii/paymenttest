package org.mytest;


/**
 * Class for start application
 *
 * Created by Pavel.Kvapil on 3.3.2015.
 */
public class Start {

    /**
     * Aplication start method
     * @param args
     */
    public static void main(String[] args){

        final Application application = new Application();
        application.run();
    }
}
