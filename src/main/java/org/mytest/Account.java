package org.mytest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.apache.commons.collections.CollectionUtils;


/**
 * Keeps track off payment and balance in different currencies
 * update and read are locked by readwrite lock
 *
 * Created by Pavel.Kvapil on 4.3.2015.
 */
public final class Account {

    private static final Account INSTANCE = new Account();

    private static Map<String,BigDecimal> accountsMap = new HashMap<String, BigDecimal>();
    private static List<Payment> payments = new ArrayList<Payment>();

    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

    private final Lock readLock = readWriteLock.readLock();
    private final Lock writeLock = readWriteLock.writeLock();

    private Account() {
    }

    public static Account getInstance() {
        return INSTANCE;
    }

    /**
     * Update currencies and add payments
     * @param inputPayments input payments to store
     */
    public void updateBalance(final List<Payment> inputPayments){

        if(CollectionUtils.isEmpty(inputPayments)){
            return;
        }
        writeLock.lock();
        try {
            for(Payment payment : inputPayments){

                payments.add(payment);
                final BigDecimal oldAmount = accountsMap.get(payment.getCurrency());

                if(oldAmount == null){
                    accountsMap.put(payment.getCurrency(), payment.getAmount());
                }else {
                    accountsMap.put(payment.getCurrency(), oldAmount.add(payment.getAmount()));
                }
            }
        }finally{
            writeLock.unlock();
        }
    }

    /**
     * Get account balance for all currencies
     *
     * @return balance for currencies
     */
    public Map<String,BigDecimal> getCurrenciesBalance(){
        readLock.lock();
        try{
            return new HashMap<String, BigDecimal>(accountsMap);
        }finally{
            readLock.unlock();
        }
    }
}
