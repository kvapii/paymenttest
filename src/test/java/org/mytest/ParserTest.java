package org.mytest;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

/**
 * Created by Pavel.Kvapil on 8.3.2015.
 */
public class ParserTest {

    private Parser parser = new Parser();

    @Test
    public void testParse_null(){

        final List<String> invalidInput = parser.parse(null);

        assertNull(invalidInput);
    }

    @Test
    public void testParse_emtpy(){

        final List<String> invalidInput = parser.parse(new ArrayList<String>());

        assertNull(invalidInput);
    }

    @Test
    public void testParse_correctInput(){

        final List<String> input = new ArrayList<String>();
        input.add("USD 100");
        input.add("GBR 100");

        final List<String> invalidInput = parser.parse(input);
        assertTrue(invalidInput.isEmpty());
    }
}
