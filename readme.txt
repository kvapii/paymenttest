Jde o maven projekt.
T��da Start obsahuje metodu main pro spusteni.
Po spu�t�n� je na obrazovce dotaz jestli chcete zad�vat data, tak stisknout "Y".
To je z d�vodu konkurence mezi vl�kny. Aby b�hem zad�v�n� nedo�lo k v�pisu dat.
V p��pad� zad�n� �patn�ho vstupu se zobraz� z�znamy, kter� jsou �patn�. Detail se nevypisuje.
Data je nutn� zad�vat ve form�tu nap�:
USD-100
USD 100
USD -100
Posledn� je pro z�porn� ��sla.
Po dokon�en� zad�v�n� na pr�zdn�m ��dku stisknout enter a data se zpracuj�.

Cuncurency:
Je �e�ena na dvou m�stech.
1.Na vstupu. Pokud u�ivatel zad�v� data, tak se data do konzole nevypisuj�.
2.Na objektu, kter� data dr��. Kv�li konzistenci dat.
  
P�esto, �e prvn� p��pad, �e�� i konzistenci, tak jsem cht�l nechat zodpov�dnost za konzistenci na objektu, kter� data dr��.
Na vstupu je konkurence �e�ena jen pro p��pad zad�v�n� dat z konzole. 

V obou p��padech je pou�it readwritelock.

V�pis dat je prov�d�n v pravideln�ch intervalech pokud nejsou data zapisov�na do konzole.

Testy:
Pouze mal� ��st aplikace je pokryta testy co� nen� v po��dku. Je to z d�vodu nedostatku �asu.